<?php
/**
 * Columns Footer template part.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

if ( ! is_active_sidebar( 'columns-footer' ) && ! is_active_sidebar( 'columns-footer-2' ) && ! is_active_sidebar( 'columns-footer-3' ) ) {
	return;
} ?>

<div class="columns-footer">
	<div class="container">
		<div class="row">
			<?php if ( is_active_sidebar( 'columns-footer' ) ) : ?>
				<div class="col-md-4 col-xs-12">
					<?php dynamic_sidebar( 'columns-footer' ); ?>
				</div>
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'columns-footer-2' ) ) : ?>
				<div class="col-md-4 col-xs-12">
					<?php dynamic_sidebar( 'columns-footer-2' ); ?>
				</div>
			<?php endif; ?>

			<?php if ( is_active_sidebar( 'columns-footer-3' ) ) : ?>
				<div class="col-md-4 col-xs-12">
					<?php dynamic_sidebar( 'columns-footer-3' ); ?>
				</div>
			<?php endif; ?>
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .columns-footer -->
