<?php
/**
 * Topbar template part.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */
?>
<div class="topbar">
	<div class="container">
		<?php if ( is_active_sidebar( 'topbar' ) ) {
			dynamic_sidebar( 'topbar' );
		} ?>
	</div>
</div>
