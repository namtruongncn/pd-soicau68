<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					pd_postview_set( get_the_ID() );

					get_template_part( 'template-parts/content', 'single' );

					the_post_navigation( array(
						'prev_text' => '← ' . get_the_title(),
						'next_text' => get_the_title() . ' →',
					) );

					if ( is_active_sidebar( 'under-singular' ) ) {
						dynamic_sidebar( 'under-singular' );
					}

				endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
